import UIKit
import Foundation

//Problem
//You have  rectangles. A rectangle is golden if the ratio of its sides is in between , both inclusive. Your task is to find the number of golden rectangles.
//
//Input format
//
//First line: Integer  denoting the number of rectangles
//Each of the  following lines: Two integers  denoting the width and height of a rectangle
//Output format
//
//Print the answer in a single line.
//Constraints
//
//
//
//Sample Input
//5
//10 1
//165 100
//180 100
//170 100
//160 100
//
//Sample Output
//3
//Time Limit: 1
//Memory Limit: 256
//Source Limit:
//Explanation
//There are three golden rectangles: (165, 100), (170, 100), (160, 100).

typealias rectangle = (width: Int, height: Int)

func findGoldenRectangle(_ arr: [rectangle]){
    
    var counterGoldenRectangles = 0
    let range = 1.6...1.7
    for rectangle in arr{
        let rectangleRatio = CGFloat(rectangle.width)/CGFloat(rectangle.height)
        if range.contains(rectangleRatio){
            counterGoldenRectangles += 1
        }
    }
    print(counterGoldenRectangles)
}

var rectangle1 : rectangle = rectangle(width: 10, height:1)
var rectangle2 : rectangle = rectangle(width: 165, height:100)

findGoldenRectangle([rectangle1, rectangle2, rectangle(width: 180, height:100), rectangle(width: 170, height:100), rectangle(width: 160, height:100)])
